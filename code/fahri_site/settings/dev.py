from .base import *

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "django-insecure-zuz5y%h)a71csoh@q*##f5y78wl8tlrbc44uv2zy&&m210iae0"

# SECURITY WARNING: define the correct hosts in production!
ALLOWED_HOSTS = ["*"]

EMAIL_BACKEND = "django.core.mail.backends.console.EmailBackend"

DATABASES = {
    'default': {
        'ENGINE'  : 'django.db.backends.mysql',
        'NAME'    : 'coc_fahri',
        'USER'    : 'root',
        'PASSWORD': 'tiger',
        'HOST'    : 'maria10',
        'PORT'    : '3306',
    }
}

try:
    from .local import *
except ImportError:
    pass
