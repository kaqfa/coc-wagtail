from django.db import models

from wagtail.models import Page
from wagtail.fields import RichTextField
from blog.models import BlogPage, BlogIndexPage

# import MultiFieldPanel:
from wagtail.admin.panels import FieldPanel, MultiFieldPanel


class HomePage(Page):
    icon = models.ForeignKey(
        "wagtailimages.Image", 
        null=True, blank=True, on_delete=models.SET_NULL, 
        related_name="icon", help_text="Icon website",
    )
    image = models.ForeignKey(
        "wagtailimages.Image",
        null=True,
        blank=True,
        on_delete=models.SET_NULL,
        related_name="+",
        help_text="Logo website",
    )
    hero_text = models.CharField(
        blank=True,
        max_length=255, help_text="Write an introduction for the site"
    )
    featured = models.ForeignKey(BlogPage, on_delete=models.SET_NULL, null=True, blank=True)

    fb_name = models.CharField("Facebook Username", max_length=250, default='', blank=True)
    ig_name = models.CharField("Instagram Username", max_length=250, default='', blank=True)
    x_name = models.CharField("X / Twitter Username", max_length=250, default='', blank=True)
    tt_name = models.CharField("Tik Tok Username", max_length=250, default='', blank=True)

    footer_1 = RichTextField(blank=True, features=['bold', 'italic', 'link', 'ol', 'ul', 'blockquote'])
    footer_2 = RichTextField(blank=True, features=['bold', 'italic', 'link', 'ol', 'ul', 'blockquote'])

    # modify your content_panels:
    content_panels = Page.content_panels + [
        FieldPanel("featured"),
        FieldPanel("icon"),
        FieldPanel("image"),
        FieldPanel("hero_text"),
        FieldPanel("footer_1"),
        FieldPanel("footer_2"),
        MultiFieldPanel(
            [
                FieldPanel("fb_name"),
                FieldPanel("ig_name"),
                FieldPanel("x_name"),
                FieldPanel("tt_name"),
            ],
            heading="Media Sosial",
        ),
    ]

    # @property
    # def featured_post(self):
    #     return BlogPage.objects.filter(featured=True).first()
    
    @property
    def categories(self):
        return BlogIndexPage.objects.descendant_of(self).live()
