# Generated by Django 4.2.13 on 2024-07-02 09:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wagtailimages', '0026_delete_uploadedimage'),
        ('home', '0006_remove_homepage_hero_cta_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='homepage',
            name='icon',
            field=models.ForeignKey(blank=True, help_text='Icon website', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='icon', to='wagtailimages.image'),
        ),
        migrations.AlterField(
            model_name='homepage',
            name='image',
            field=models.ForeignKey(blank=True, help_text='Logo website', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='wagtailimages.image'),
        ),
    ]
