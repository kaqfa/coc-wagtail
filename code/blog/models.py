from django import forms
from django.db import models

from modelcluster.fields import ParentalKey, ParentalManyToManyField
from modelcluster.contrib.taggit import ClusterTaggableManager
from taggit.models import TaggedItemBase

from wagtail.snippets.models import register_snippet
from wagtail.models import Page, Orderable
from wagtail.fields import RichTextField
from wagtail.admin.panels import FieldPanel, InlinePanel, MultiFieldPanel
from wagtail.search import index


class BlogIndexPage(Page):
    intro = RichTextField(blank=True)

    def get_context(self, request):
        context = super().get_context(request)
        blogpages = self.get_children().live().order_by('-first_published_at')
        context['blogpages'] = blogpages
        return context

    content_panels = Page.content_panels + [
        FieldPanel('intro')
    ]

    @property
    def latest_post(self):
        return self.get_children().live().order_by('-first_published_at').specific()[:3]
    
    @property
    def num_posts(self):
        return self.get_children().live().count()



class BlogPageTag(TaggedItemBase):
    content_object = ParentalKey(
        'BlogPage',
        null=True, blank=True,
        related_name='tagged_items',
        on_delete=models.SET_NULL
    )

    def get_context(self, request):
        tag = request.GET.get('tag')
        blogpages = BlogPage.objects.filter(tags__name=tag)
        context = super().get_context(request)
        context['blogpages'] = blogpages
        return context


class BlogPage(Page):
    date = models.DateField("Post date")
    intro = models.CharField(max_length=250)
    summary = models.TextField(null=True, blank=True)
    image = models.ForeignKey(
        'wagtailimages.Image', on_delete=models.SET_NULL, related_name='blog_image',
        null=True, blank=True
    )
    body = RichTextField(blank=True, 
                         features=['h2', 'h3', 'bold', 'italic', 'link', 'code', 'hr',
                                   'ol', 'ul', 'blockquote', 'image', 'embed', 'media'])
    tags = ClusterTaggableManager(through=BlogPageTag, blank=True)

    search_fields = Page.search_fields + [
        index.SearchField('intro'),
        index.SearchField('body'),
    ]

    content_panels = Page.content_panels + [
        FieldPanel('intro'),
        FieldPanel('body'),
        FieldPanel('image'),
        FieldPanel('summary'),
        MultiFieldPanel([
            FieldPanel('date'),
            FieldPanel('tags'),
        ], heading="Blog information"),
    ]

    @property
    def related_posts(self):
        return BlogPage.objects.exclude(id=self.id)[:3]